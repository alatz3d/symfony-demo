# README #

### Para que sirve este repositorio? ###

###### Es una <strong style="font-size: 110%; color: blue">Demo de aplicación hecha en Symfony 4.4.5 LTS</strong> que simula un protototipo de web con usuarios que se subscriben a cursos impartidos por distintos centros repartidos en varios países. 

Los usuarios podrán consultar los materiales de cada curso cuando se active el acceso a los mismos desde un panel de administración una vez se hayan suscrito.

Desde el panel de control se podrán gestionar todos los contenidos, usuarios etc. ( /admin )

### Como configurar el repositorio? ###

* Instalar el proyecto:
`composer install`

* Configuración:
`.env` (opcional: prod/dev)

* Dependencias:
    * PHP 7 y Composer
    * MariaDB 10 o MySQL 5

* Configuración de Base de datos:
    Crear un usuario 'symfony' con password 'symfony' con privilegios para la siguiente base de datos:
    `CREATE DATABASE `demo_sf_app` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;`

    Por último cargaremos las tablas y los datos de ejemplo con los siguientes comandos: _(symfony console y php bin/console son equivalentes)_
    `symfony console doctrine:migrations:migrate`
    `php bin/console doctrine:fixtures:load`

    Y crearemos los usuarios necesarios: "admin::test" y "demo::test"
    `symfony console fos:user:create demo`
    `symfony console fos:user:promote demo` ROLE_ADMIN

* Como ejecutar un test unitario:
    Para ello... make:unit-test, make:functional-test

* Instruciones para el Deploy:
    Clonar este repositorio y seguir las instrucciones de instalación y configuración indicadas previamente.


##### NOTAS: DEMO Symfony 
Una vez instalados y configurados PHP, Composer y Symfomy creamos una aplicación con soporte LTS con el siguiente comando:

    symfony new --full proyecto --version=lts 
    o:
    composer create-project symfony/website-skeleton:^4.4 my_project_name 

Para usar anotaciones:
    ` composer require annotations `

## Entidades:      
    # Pais
        nombre: string 100
        locale: string 5
    #Centro
        nombre: string 255
        telefono: string 15
        dirección: string 255        
        pais: relation.ManyToOne (Pais.centros)        
    
    # Curso
        activo: boolean
        nombre: string 255
        descripcion: text
        horas: smallint
        precio: float
        centro: relation.ManyToOne (Centro.cursos)
    # Material
        nombre: string 255
        fecha: date
        curso: relation.ManyToOne (Curso.materiales)
        ? fichero: vich
        https://symfony.com/doc/current/bundles/EasyAdminBundle/integration/vichuploaderbundle.html

    # Usuario: FOSUserBundle
    
    # Suscripcion
        activo: boolean
        fecha: date
        curso: relation.ManyToOne (Curso)
        usuario: relation.ManyToOne (User)
        

php bin/console make:migration
symfony console doctrine:migrations:migrate

FIXTURES

composer require --dev orm-fixtures
php bin/console doctrine:fixtures:load



 BUNDLES 
 - EasyAdminBundle:
    `composer require admin`

 - FOSUserBundle     
    `composer require symfony/serializer`
    `composer require friendsofsymfony/user-bundle "~2.0@dev" `

 - VichUploaderBundle
    ...

