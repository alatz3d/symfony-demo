<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Pais;
use App\Entity\Centro;
use App\Entity\Curso;
use App\Entity\Material;
//use DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $paisES = new Pais();
        $paisES->setNombre('España');
        $paisES->setLocale('es');
        $manager->persist($paisES);

        $paisUK = new Pais();
        $paisUK->setNombre('Inglaterra');
        $paisUK->setLocale('en');
        $manager->persist($paisUK);

        $centro = new Centro();
        $centro->setNombre('Academia Educa');
        $centro->setTelefono('+3491456123');
        $centro->setDireccion('C/ Centro 2, Madrid.');
        $centro->setPais($paisES);
        $manager->persist($centro);

        $centro2 = new Centro();
        $centro2->setNombre('Howard´s English School');
        $centro2->setTelefono('+33021368232');
        $centro2->setDireccion('Hollytop avenue 7, London. UK');
        $centro2->setPais($paisUK);
        $manager->persist($centro2);

        $curso = new Curso();
        $curso->setActivo(true);
        $curso->setNombre("Curso avanzado de programación en PHP");
        $curso->setDescripcion("Si desea mejorar sus habilidades como desarrollador en PHP, no encontrará un curso mejor con materiales extra incluidos.");
        $curso->setHoras(200);
        $curso->setPrecio(595.95);
        $curso->setCentro($centro);
        $manager->persist($curso);

        $curso2 = new Curso();
        $curso2->setActivo(true);
        $curso2->setNombre("Advanced Level English Course");
        $curso2->setDescripcion("Get your degree in a comfortable way. Best course ever.");
        $curso2->setHoras(600);
        $curso2->setPrecio(995.95);
        $curso2->setCentro($centro2);
        $manager->persist($curso2);

        $curso3 = new Curso();
        $curso3->setActivo(true);
        $curso3->setNombre("Curso avanzado de programación en JS");
        $curso3->setDescripcion("Si desea mejorar sus habilidades como desarrollador en JS, no encontrará un curso mejor con materiales extra incluidos.");
        $curso3->setHoras(200);
        $curso3->setPrecio(595.95);
        $curso3->setCentro($centro);
        $manager->persist($curso3);

        $material = new Material();
        $material->setNombre("Introducción a PHP. Parte primera");
        $material->setFecha(new \DateTime(date("Y-m-d")));
        $material->setCurso($curso);
        //$material->setFilePDF
        $manager->persist($material);

        $material = new Material();
        $material->setNombre("Patrones de diseño y estucturas de datos.");
        $material->setFecha(new \DateTime(date("Y-m-d")));
        $material->setCurso($curso);
        //$material->setFilePDF
        $manager->persist($material);

        $material = new Material();
        $material->setNombre("Exercises and reading. Advanced level course.");
        $material->setFecha(new \DateTime(date("Y-m-d")));
        $material->setCurso($curso2);
        //$material->setFilePDF
        $manager->persist($material);

        $manager->flush();
    }
}
