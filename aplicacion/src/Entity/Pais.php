<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaisRepository")
 */
class Pais
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $locale;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Centro", mappedBy="pais")
     */
    private $centros;

    public function __construct()
    {
        $this->centros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return Collection|Centro[]
     */
    public function getCentros(): Collection
    {
        return $this->centros;
    }

    public function addCentro(Centro $centro): self
    {
        if (!$this->centros->contains($centro)) {
            $this->centros[] = $centro;
            $centro->setPais($this);
        }

        return $this;
    }

    public function removeCentro(Centro $centro): self
    {
        if ($this->centros->contains($centro)) {
            $this->centros->removeElement($centro);
            // set the owning side to null (unless already changed)
            if ($centro->getPais() === $this) {
                $centro->setPais(null);
            }
        }

        return $this;
    }

    public function __toString() {
        return $this->getNombre();
    }
}
