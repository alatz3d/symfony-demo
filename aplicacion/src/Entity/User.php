<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use FOS\UserBundle\Model\User as BaseUser;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Suscripcion", mappedBy="Usuario", orphanRemoval=true)
     */
    private $suscripciones;

    public function __construct()
    {
        parent::__construct();
        $this->suscripciones = new ArrayCollection();
        // your own logic
    }

    /**
     * @return Collection|Suscripcion[]
     */
    public function getSuscripciones(): Collection
    {
        return $this->suscripciones;
    }

    public function addSuscripcione(Suscripcion $suscripcione): self
    {
        if (!$this->suscripciones->contains($suscripcione)) {
            $this->suscripciones[] = $suscripcione;
            $suscripcione->setUsuario($this);
        }

        return $this;
    }

    public function removeSuscripcione(Suscripcion $suscripcione): self
    {
        if ($this->suscripciones->contains($suscripcione)) {
            $this->suscripciones->removeElement($suscripcione);
            // set the owning side to null (unless already changed)
            if ($suscripcione->getUsuario() === $this) {
                $suscripcione->setUsuario(null);
            }
        }

        return $this;
    }
}




// use Symfony\Component\Security\Core\User\UserInterface;

// /**
//  * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
//  */
// class User implements UserInterface
// {
//     /**
//      * @ORM\Id()
//      * @ORM\GeneratedValue()
//      * @ORM\Column(type="integer")
//      */
//     private $id;

//     /**
//      * @ORM\Column(type="string", length=180, unique=true)
//      */
//     private $email;

//     /**
//      * @ORM\Column(type="json")
//      */
//     private $roles = [];

//     /**
//      * @var string The hashed password
//      * @ORM\Column(type="string")
//      */
//     private $password;

//     public function getId(): ?int
//     {
//         return $this->id;
//     }

//     public function getEmail(): ?string
//     {
//         return $this->email;
//     }

//     public function setEmail(string $email): self
//     {
//         $this->email = $email;

//         return $this;
//     }

//     /**
//      * A visual identifier that represents this user.
//      *
//      * @see UserInterface
//      */
//     public function getUsername(): string
//     {
//         return (string) $this->email;
//     }

//     /**
//      * @see UserInterface
//      */
//     public function getRoles(): array
//     {
//         $roles = $this->roles;
//         // guarantee every user at least has ROLE_USER
//         $roles[] = 'ROLE_USER';

//         return array_unique($roles);
//     }

//     public function setRoles(array $roles): self
//     {
//         $this->roles = $roles;

//         return $this;
//     }

//     /**
//      * @see UserInterface
//      */
//     public function getPassword(): string
//     {
//         return (string) $this->password;
//     }

//     public function setPassword(string $password): self
//     {
//         $this->password = $password;

//         return $this;
//     }

//     /**
//      * @see UserInterface
//      */
//     public function getSalt()
//     {
//         // not needed when using the "bcrypt" algorithm in security.yaml
//     }

//     /**
//      * @see UserInterface
//      */
//     public function eraseCredentials()
//     {
//         // If you store any temporary, sensitive data on the user, clear it here
//         // $this->plainPassword = null;
//     }

// }
