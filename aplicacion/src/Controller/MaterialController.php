<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Curso;
use App\Entity\Material;

class MaterialController extends AbstractController
{
    /**
     * @param Int $id_curso
     *      
     * @Route("/material/{id_curso}", requirements={"id_curso" = "\d+"}, name="material")
     */
    public function index(Int $id_curso, EntityManagerInterface $em)
    {
        $repository = $em->getRepository(Curso::class);
        $curso = $repository->findBy(array('id' => $id_curso))[0];

        $repository = $em->getRepository(Material::class);
        $materiales = $repository->findBy(array('curso' => $curso));


        return $this->render('material/index.html.twig', [
            'curso' => $curso,
            'materiales' => $materiales,
        ]);
    }
}
