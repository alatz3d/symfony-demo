<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Pais;
use App\Entity\Centro;
use Symfony\Component\HttpFoundation\Request;

class CentrosController extends AbstractController
{
    /**
     * @Route("/centros", name="centros")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        // $_GET parameters
        $ID_pais = $request->query->get('pais');
        

        $repository = $em->getRepository(Pais::class);
        $pais = $repository->findBy(array('id' => $ID_pais), null)[0];

        $repository = $em->getRepository(Centro::class);        
        $centros = $repository->findBy(array('pais' => $pais));

        return $this->render('centros/index.html.twig', [
            'controller_name' => 'CentrosController',
            'id_pais' => $ID_pais,
            'pais' => $pais,
            'centros' => $centros,
        ]);
    }
}
