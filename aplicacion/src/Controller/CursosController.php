<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Pais;
use App\Entity\Centro;
use App\Entity\Curso;
use App\Entity\User;
use App\Entity\Suscripcion;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CursosController extends AbstractController
{
    /**
     * @Route("/cursos", name="cursos")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {

        // $_GET parametros y Objs
        $ID_pais = $request->query->get('pais');
        $ID_centro = $request->query->get('centro');

        $repository = $em->getRepository(Pais::class);
        $pais = $repository->findBy(array('id' => $ID_pais))[0];

        $repository = $em->getRepository(Centro::class);
        $centro = $repository->findBy(array('id' => $ID_centro))[0];

        $repository = $em->getRepository(Curso::class);
        $cursos = $repository->findBy(array('centro' => $centro));

        $repository = $em->getRepository(Suscripcion::class);
        $usuario = $this->getUser();
        if ($usuario != null) {
            $suscripciones = $repository->findBy(array('Usuario' => $usuario->getId() ));
            foreach ($suscripciones as $suscrip) {
                $curso = $suscrip->getCurso();
                if ($suscrip->getActivo() == true) { $activo = 1; } else { $activo = 0; }
                $suscripCursos[] = [ $curso->getId(), $activo];
            }            
        } else {
            $suscripCursos = [];            
        }
        
        return $this->render('cursos/index.html.twig', [
            'controller_name' => 'CursosController',
            'id_pais' => $ID_pais,
            'pais' => $pais,
            'id_centro' => $ID_centro,
            'centro' => $centro,
            'cursos' => $cursos,
            'suscripciones' => $suscripCursos,            
        ]);
    }

    /**
     * @param Int $id_usuario
     * @param Int $id_curso
     *
     * @Route("/{id_usuario}/{id_curso}/suscribirse-curso", requirements={"id_curso" = "\d+"}, name="suscribirseCurso")
     * @return RedirectResponse
     *
     */
    public function suscribirseAction(Int $id_usuario, Int $id_curso, EntityManagerInterface $em)
    {
        $repository = $em->getRepository(User::class);
        $usuario = $repository->findBy(array('id' => $id_usuario))[0];

        $repository = $em->getRepository(Curso::class);
        $curso = $repository->findBy(array('id' => $id_curso))[0];        

        $ahora = new \DateTime(date("Y-m-d"));
        $suscripcion = new Suscripcion();
        //NOTA: Hay que activarlo desde el admin (Aprobación manual / "Pago")
        $suscripcion->setActivo(false);
        $suscripcion->setFecha($ahora);
        $suscripcion->setCurso($curso);
        $suscripcion->setUsuario($usuario);
        $em->persist($suscripcion);
        
        $em->flush();
        
        return new Response('Guardada suscripción con id '.$suscripcion->getId());
    }
}
