<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Pais;
use Doctrine\ORM\EntityManagerInterface;

class SelectorPaisController extends AbstractController
{
    /**
     * @Route("/paises", name="paises")
     */
    public function index(EntityManagerInterface $em)
    {

        $repository = $em->getRepository(Pais::class);
        $paises = $repository->findAll();

        return $this->render('selector_pais/index.html.twig', [
            'paises' => $paises,
        ]);
    }
}

